# docker-images-conan-qt

Some Docker images used for GitLab CI/CD

## Why having my own Qt conan packages

Qt have been available as conan packages since quite some time,
on the [bincrafters/conan-qt](https://github.com/bincrafters/conan-qt) repository.

Qt is now also available on [conan center](https://conan.io/center/qt).

Despite a lot of different binaries are provided,
there are chances that Qt will have to be rebuilt for a project.
In a CI/CD pipeline, this is simply not acceptable.

## Why docker images for conan packages ?

Simply because public conan registries (or registry, we not have so many)
will not accpet big packages like Qt.

# Usage

Here is a minimal example:
```yml
stages:
  - build

build_linux_gcc_debug:
  stage: build
  image: registry.gitlab.com/scandyna/docker-images-ubuntu/ubuntu-18.04-cpp-gui:latest
  script:
    - mkdir build
    - cd build
    - conan install -s build_type=Debug --build=missing ..
    - cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
    - make -j4
  artifacts:
    expire_in: 1 day
    paths:
      - build
```

To run tests that requires a X11 server running,
choose a image that has the required packages installed,
and launch the [Xdummy](https://xpra.org/trac/wiki/Xdummy) server:
```yml
stages:
  - test

test_linux_gcc_debug:
  stage: test
  image: registry.gitlab.com/scandyna/docker-images-ubuntu/ubuntu-18.04-cpp-gui:latest
  variables:
    DISPLAY: ":99"
  dependencies:
    - build_linux_gcc_debug
  before_script:
    - /etc/init.d/xdummy-server start
  script:
    - cd build
    - ctest --output-on-failure .
```

# Available images

## ubuntu-18.04-cpp-qt-widgets-conan-base

Based on `ubuntu-18.04-minimal-xserver`,
comming from [here](https://gitlab.com/scandyna/docker-images-ubuntu),
this image provides the basis for Qt Widgets Conan based images.

## ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-debug

Based on `ubuntu-18.04-cpp-qt-widgets-conan-base`,
this image provides Qt 5.14.2 compiled with Clang, libc++.

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-conan-qt/ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-debug:latest
```

This Qt version is installed with Conan.
In your conanfile, add `qt/5.14.2@bincrafters/stable` as requied.
To compile:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_qt_widgets_modules -s build_type=Debug ..
source activate.sh
cmake -DCMAKE_BUILD_TYPE=Debug ..
```
